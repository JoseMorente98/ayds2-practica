import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Frontend';
  carnet:number = 201801237;


  sumar() {
    this.carnet++;
  }

  restar() {
    this.carnet--;
  }
}
