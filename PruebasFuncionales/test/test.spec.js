const { Builder, By, Key, until } = require('selenium-webdriver');

/**
 * PRUEBAS FUNCIONALES
 * 201801237
 */
(async function sumar() {
  let driver = await new Builder().forBrowser('firefox').build();
  try {
    await driver.get('http://34.125.201.133/');

    await driver.findElement(By.css('btn btn-primary btn-lg btn-block')).click()

    let firstResult = await driver.wait(until.elementLocated(By.id('carnet')), 10000);

    console.log(await firstResult.getAttribute('textContent'));
  }
  finally {
    await driver.quit();
  }
});


(async function restar() {
  let driver = await new Builder().forBrowser('firefox').build();
  try {
    await driver.get('http://34.125.201.133/');

    await driver.findElement(By.css('btn btn-danger btn-lg btn-block')).click()

    let firstResult = await driver.wait(until.elementLocated(By.id('carnet')), 10000);

    console.log(await firstResult.getAttribute('textContent'));
  }
  finally {
    await driver.quit();
  }
});
